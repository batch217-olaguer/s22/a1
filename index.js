/*
    Create functions which can manipulate our arrays.
*/

let registeredUsers = [

    "James Jeffries",
    "Gunther Smith",
    "Macie West",
    "Michelle Queen",
    "Shane Miguelito",
    "Fernando Dela Cruz",
    "Akiko Yukihime"
];

let friendsList = [];

// ok.
/* 
    
   1. Create a function which will allow us to register into the registeredUsers list.
        - this function should be able to receive a string.
        - determine if the input username already exists in our registeredUsers array.
            -if it is, show an alert window with the following message:
                "Registration failed. Username already exists!"
            -if it is not, add the new username into the registeredUsers array and show an alert:
                "Thank you for registering!"
        - invoke and register a new user.
        - outside the function log the registeredUsers array.

*/

    function register (newUser){ 
        if (registeredUsers.includes(newUser) == false){
            registeredUsers.push(newUser);
            alert("Thank you for registering, " + newUser + "!");
            return;
        } else {
            alert("Registration failed. Username already exists!");  
        }
    }
// use console to invoke function 'register'.
// use console to log the registeredUsers Array.
// ==================================================================

// ok.
/*

    2. Create a function which will allow us to add a registered user into our friends list.
        - this function should be able to receive a string.
        - determine if the input username exists in our registeredUsers array.
            - if it is, add the foundUser in our friendList array.
                    -Then show an alert with the following message:
                        - "You have added <registeredUser> as a friend!"
            - if it is not, show an alert window with the following message:
                - "User not found."
        - invoke the function and add a registered user in your friendsList.
        - Outside the function log the friendsList array in the console.

*/
   function addFriend (userFriend){
        if (registeredUsers.includes(userFriend) == false){
            alert("User " + userFriend + " not found!");
      
        }
        if (friendsList.includes(userFriend) == true){
            alert("You are already friends with " + userFriend + ".");

        }
        else {
            friendsList.push(userFriend)
            alert("You have now added " + userFriend + " as a friend!");
            return;
        }        
   } 
// use console to invoke function 'addFriend'.
// use console to log the registeredUsers Array.
// ==================================================================

// ok.
/*

    3. Create a function which will allow us to show/display the items in the friendList one by one on our console.
        - If the friendsList is empty show an alert: 
            - "You currently have 0 friends. Add one first."
        - Invoke the function.

*/

    function displayFriendsList (friends) {
        if (friendsList.length === 0) {
            alert("You currently have 0 friends. Add one first.");
        }
        if (friendsList.length === 1) {
            console.log("Your friend is: ")
        }
        if (friendsList.length > 1) {
            console.log("Your friends are: ")
        }
        friendsList.forEach(function(friendsList){
            console.log(friendsList);
        })

    }
// use console to invoke function 'displayFriendsList'.
// use console to log the friendsList Array.
// ==================================================================

/* ok.
    4. Create a function which will display the amount of registered users in your friendsList.
        - If the friendsList is empty show an alert:
            - "You currently have 0 friends. Add one first."
        - If the friendsList is not empty show an alert:
            - "You currently have <numberOfFriends> friends."
        - Invoke the function

*/
    function displayNumberOfFriends () {
        if (friendsList.length === 0) {
            alert("You currently have 0 friends. Add one first.");
        }
        if (friendsList.length === 1) {
            console.log("You currently have 1 friend." );
            alert("You currently have 1 friend." );
        }
        if (friendsList.length > 1) {
            console.log("You currently have " + friendsList.length + " friends." );
            alert("You currently have " + friendsList.length + " friends." );
        }
    }
// use console to invoke function 'displayNumberOfFriends'.
// use console to log the friendsList.length to check for number of friends.
// ==================================================================

/* ok.
    5. Create a function which will delete the last registeredUser you have added in the friendsList.
        - If the friendsList is empty show an alert:
            - "You currently have 0 friends. Add one first."
        - Invoke the function.
        - Outside the function log the friendsList array.

*/

    function removeLastAddedFriend () {
        if (!friendsList) {
            friendsList.pop();
            alert("Removed last added friend from friends list.")
            console.log("Removed " + friendsList.length-1 + " as friend.");
        } else {
            alert("You currently have 0 friends to remove. Add one first.");
        }
    }
// use console to invoke function 'removeLastAddedFriend'.
// use console to log the friendsList to check friends List array.
// ==================================================================

/*
    Stretch Goal:

    Instead of only deleting the last registered user in the friendsList delete a specific user instead.
        -You may get the user's index.
        -Then delete the specific user with splice().

*/
    function removeFriend () {
        console.log("Use the list below to chose which friend/s you want to remove. Use the function 'unFriend () to remove the selected user from your friends list.")
        friendsList.sort();
    }

    function unFriend () {
        friendsList.splice()
    }
